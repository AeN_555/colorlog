# ColorLog (Colored Log)

![Logo](./logo.png "ColorLog logo")

ColorLog is a package for Go programs to provide a simple and efficient log system

![Example of colored logs](./example.png)

### Requierements

The package depends on [TermColor](https://gitlab.com/AeN_555/termcolor "Link to TermColor").

Obviously, it also requires [Go](https://golang.org/ "Link to Golang") itself.

### Installation

ColorLog use Go module system, so you don't need to install it to be able to use it.
Just add the necessary imports in your import list like in the example below.

### Example

```go
package main

import (
	log "gitlab.com/AeN_555/colorlog"
	"os"
)

func main() {
	log.Information("It works !")
	log.Warning("Oh ?")

	_, err := os.Open("./doesnt/exist")
	log.Error(err, "Bye !")
}
```

### Build and result

```bash
$ go build .
$ ./myBinaryWithLogs
```

![Result of the example](./result.png)

### Compatibility warning

ColorLogs use TermColor to insert colors so the result depends of the current terminal and its capabilities.

See the "Compatibility warning" section of [TermColor](https://gitlab.com/AeN_555/termcolor "Link to TermColor") for more informations.
