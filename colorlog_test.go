package colorlog_test

import (
	log "gitlab.com/AeN_555/colorlog"
)

// To test if colors can be enabled and disabled
func TestColor(t *testing.T) {
	if log.Color() != true {
		t.Errorf("default color value is not \"true\"")
	}

	log.SetColor(false)
	if log.Color() != false {
		t.Errorf("setting color to \"false\" failed")
	}

	log.SetColor(true)
	if log.Color() != true {
		t.Errorf("setting color to \"true\" failed")
	}
}

// To test if log level can be changed
func TestLogLevel(t *testing.T) {
	if log.Level() != 3 {
		t.Errorf("default log level value is not \"3\"")
	}

	log.SetLevel(0)
	if log.Level() != 0 {
		t.Errorf("setting log level to \"0\" (no logs) failed")
	}

	log.SetColor(5)
	if log.Level() != 5 {
		t.Errorf("setting log level to \"5\" (full logs) failed")
	}

	log.SetColor(3)
	if log.Level() != 3 {
		t.Errorf("setting log level to \"3\" (default logs) failed")
	}
}
