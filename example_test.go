package colorlog_test

import (
	log "gitlab.com/AeN_555/colorlog"
)

// The common usage of colorlogs (default/release mode : colors and no Debug or Verbose)
func Example() {
	log.Information("starting ...") // displayed
	// ...
	log.Debug("x: 0, y: 1, z:2")                  // not displayed
	log.Verbose("moving object to x=0, y=1, z=2") // not displayed
	// ...
	log.Warning("unknwon value ", 0, ". Discard event.") // displayed
	//...
	log.Error(nil, "failed to do stuff") // displayed and stopped the program
	//...
	log.Information("finishing ...") // not displayed because an error stop the program
}

// The usage where logs are disabled (even errors are not displayed)
func Example_disableLog() {
	log.SetLevel(0)

	log.Information("starting ...") // not displayed
	// ...
	log.Debug("x: 0, y: 1, z:2")                  // not displayed
	log.Verbose("moving object to x=0, y=1, z=2") // not displayed
	// ...
	log.Warning("unknwon value ", 0, ". Discard event.") // not displayed
	//...
	log.Error(nil, "failed to do stuff") // not displayed but stopped the program
	//...
	log.Information("finishing ...") // not displayed because an error stop the program
}

// The usage where every logs are displayed and additionnal data are also displayed
func Example_fullDebug() {
	log.SetLevel(5)

	log.Information("starting ...") // displayed
	// ...
	log.Debug("x: 0, y: 1, z:2")                  // displayed
	log.Verbose("moving object to x=0, y=1, z=2") // displayed
	// ...
	log.Warning("unknwon value ", 0, ". Discard event.") // displayed
	//...
	log.Error(nil, "failed to do stuff") // displayed but stopped the program
	//...
	log.Information("finishing ...") // not displayed because an error stop the program
}

// The usage where logs have no colors (like a release).
func Example_noColor() {
	log.SetColor(false)

	log.Information("starting ...") // displayed (but no colors)
	// ...
	log.Debug("x: 0, y: 1, z:2")                  // not displayed (but no colors)
	log.Verbose("moving object to x=0, y=1, z=2") // not displayed (but no colors)
	// ...
	log.Warning("unknwon value ", 0, ". Discard event.") // displayed (but no colors)
	//...
	log.Error(nil, "failed to do stuff") // displayed (but no colors) but stopped the program
	//...
	log.Information("finishing ...") // not displayed because an error stop the program
}

// The common usage of the debug log.
func ExampleDebug() {
	log.SetLevel(5) // to see the log

	myVar := struct {
		id    string
		value int
	}{"UID", 10}
	log.Debug("debugging variable ", myVar)
	// YYYY/MM/DD hh:mm:ss  D  example_test.go:77 debuggin {UID 10}
}

// The common usage of the verbose log.
func ExampleVerbose() {
	log.SetLevel(4) // to see the log

	log.Verbose("doing stuff very often with value ", 42, " so verbose")
	// YYYY/MM/DD hh:mm:ss  V  doing stuff very often with value 42 so verbose
}

// The advanced usage of the verbose log.
func ExampleVerbose_moreContext() {
	log.SetLevel(5) // if log level is at least 5, then additionnal data are logged (file and line number)

	log.Verbose("doing stuff very often with value ", 42, " so verbose")
	// YYYY/MM/DD hh:mm:ss  V  example_test.go:93 doing stuff very often with value 42 so verbose
}

// The common usage of the information log.
func ExampleInformation() {
	log.Information("doing stuff")
	// YYYY/MM/DD hh:mm:ss  I  doing stuff
}

// The advanced usage of the information log.
func ExampleInformation_moreContext() {
	log.SetLevel(5) // if log level is at least 5, then additionnal data are logged (file and line number)

	log.Information("doing more stuff")
	// YYYY/MM/DD hh:mm:ss  I  example_test.go:107 doing more stuff
}

// The common usage of the warning log.
func ExampleWarning() {
	log.Warning("should not happen but not critical")
	// YYYY/MM/DD hh:mm:ss  W  should not happen but not critical
}

// The advanced usage of the warning log.
func ExampleWarning_moreContext() {
	log.SetLevel(5) // if log level is at least 5, then additionnal data are logged (file and line number)

	log.Warning("should not happen either but not critical")
	// YYYY/MM/DD hh:mm:ss  W  example_test.go:121 should not happen either but not critical
}

// The common usage of the error log.
func ExampleError() {
	_, err := os.Open("./doesnt/exist")
	log.Error(err, "critical state, cannot continue")
	// YYYY/MM/DD hh:mm:ss  W  example_test.go:128 critical state, cannot continue
	//
	// * Error:
	// open ./doesnt/exist: No such file or directory
	//
	// * Stack:
	// ...
}

// The advanced usage of the error log.
func ExampleError_errorNil() {
	log.Error(nil, "critical state, cannot continue")
	// YYYY/MM/DD hh:mm:ss  W  example_test.go:140 critical state, cannot continue
	//
	// * Stack:
	// ...
}
