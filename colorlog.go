// Package colorlog provides a log system with many improvements like date, hour, file, line and color.
//
// It always provides the following informations : date, hour, log type and the message itself. But additionnal
// data can be displayed if verbosity is changed. Additionnal data are : file and line number.
package colorlog

import (
	"fmt"
	"gitlab.com/AeN_555/termcolor"
	"gitlab.com/AeN_555/termcolor/colors8"
	"gitlab.com/AeN_555/termcolor/format"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strconv"
)

const (
	idDebug       string = " D "
	idVerbose     string = " V "
	idInformation string = " I "
	idWarning     string = " W "
	idError       string = " E "

	titleBegin string = "* "
	titleEnd   string = ":"
	titleError string = "Error"
	titleStack string = "Stack"
)

var (
	color bool = true
	level uint = 3

	styleDebug       termcolor.Styler
	styleVerbose     termcolor.Styler
	styleInformation termcolor.Styler
	styleWarning     termcolor.Styler
	styleError       termcolor.Styler
	styleFileAndLine termcolor.Styler
	styleTitle       termcolor.Styler

	loggerDebug       *log.Logger
	loggerVerbose     *log.Logger
	loggerInformation *log.Logger
	loggerWarning     *log.Logger
	loggerError       *log.Logger
)

// To initialize the package.
func init() {
	styleDebug = &termcolor.Style{
		TextColor:       colors8.Black,
		BackgroundColor: colors8.LightGray,
		Attributes:      []format.Format{format.Normal},
	}
	styleVerbose = &termcolor.Style{
		TextColor:       colors8.Black,
		BackgroundColor: colors8.Cyan,
		Attributes:      []format.Format{format.Normal},
	}
	styleInformation = &termcolor.Style{
		TextColor:       colors8.Black,
		BackgroundColor: colors8.Green,
		Attributes:      []format.Format{format.Normal},
	}
	styleWarning = &termcolor.Style{
		TextColor:       colors8.Black,
		BackgroundColor: colors8.Yellow,
		Attributes:      []format.Format{format.Normal},
	}
	styleError = &termcolor.Style{
		TextColor:       colors8.Black,
		BackgroundColor: colors8.Red,
		Attributes:      []format.Format{format.Normal},
	}
	styleFileAndLine = &termcolor.Style{
		TextColor:       colors8.LightGray,
		BackgroundColor: colors8.Default,
		Attributes:      []format.Format{format.Normal},
	}
	styleTitle = &termcolor.Style{
		TextColor:       colors8.Default,
		BackgroundColor: colors8.Default,
		Attributes:      []format.Format{format.Bold, format.Underline},
	}

	loggerDebug = log.New(os.Stdout, "", log.Ldate|log.Ltime)
	loggerVerbose = log.New(os.Stdout, "", log.Ldate|log.Ltime)
	loggerInformation = log.New(os.Stdout, "", log.Ldate|log.Ltime)
	loggerWarning = log.New(os.Stdout, "", log.Ldate|log.Ltime)
	loggerError = log.New(os.Stderr, "", log.Ldate|log.Ltime)
}

// To know if the color is enabled for all logs (default is true).
//
// See Also
//
// func SetColor(enabled bool)
func Color() bool {
	return color
}

// To enable or disable the color for the logs.
//
// See Also
//
// func Color() bool
func SetColor(enabled bool) {
	color = enabled
}

// To know if the log level for all logs (default is 3).
//
// See Also
//
// func SetLevel(newLevel uint)
func Level() uint {
	return level
}

// To set the log level for the logs (0 to 5).
//
// If log level is at least 5, additionnal data are displayed for every logs (file and line number).
//
// See Also
//
// func Level() uint
func SetLevel(newLevel uint) {
	level = newLevel
}

// To display a debug log (if log level is at least 5).
func Debug(msg ...interface{}) {
	if level > 4 {
		loggerDebug.Println(endPrefix(idDebug, styleDebug) + fileAndLine(false) + fmt.Sprint(msg...))
	}
}

// To display a verbose log (if log level is at least 4).
func Verbose(msg ...interface{}) {
	if level > 3 {
		loggerVerbose.Println(endPrefix(idVerbose, styleVerbose) + fileAndLine(false) + fmt.Sprint(msg...))
	}
}

// To display an information log (if log level is at least 3).
func Information(msg ...interface{}) {
	if level > 2 {
		loggerInformation.Println(endPrefix(idInformation, styleInformation) + fileAndLine(false) + fmt.Sprint(msg...))
	}
}

// To display a warning log (if log level is at least 2).
func Warning(msg ...interface{}) {
	if level > 1 {
		loggerWarning.Println(endPrefix(idWarning, styleWarning) + fileAndLine(false) + fmt.Sprint(msg...))
	}
}

// To display an error, an error log and the call stack (if log level is at least 1). Additionnal data (file and line
// number) are displayed at any log level (if the error itself is already displayed, so at least 1) because of the very
// importance of an error. The error to display can be "nil"; in that case, only the error log and the call stack will
// be displayed. In any case, a call to that function will stop the program.
func Error(err error, msg ...interface{}) {
	if level > 0 {
		loggerError.Println(endPrefix(idError, styleError) + fileAndLine(true) + fmt.Sprint(msg...))

		if err != nil {
			fmt.Println("\n" + title(titleError))
			fmt.Println(err)
		}

		fmt.Println("\n" + title(titleStack))
		debug.PrintStack()
	}
	os.Exit(1)
}

// To return the string that follow the log prefix.
func endPrefix(idLog string, style termcolor.Styler) string {
	if color == true {
		idLog = termcolor.String(style, idLog)
	}

	return idLog + " "
}

// To return the file name and the line where the log came from.
func fileAndLine(force bool) string {
	if force == false && level < 5 {
		return ""
	}

	_, file, line, _ := runtime.Caller(2)
	res := filepath.Base(file) + ":" + strconv.FormatInt(int64(line), 10)
	if color == true {
		res = termcolor.String(styleFileAndLine, res)
	}
	return res + " "
}

// To format a title.
func title(str string) string {
	str = titleBegin + str + titleEnd
	if color == true {
		str = termcolor.String(styleTitle, str)
	}

	return str
}
